import React, { Component } from "react";
import PropTypes from "prop-types";
import { AppWrapper } from "../public/AppWrapper";
import { PATH } from "../../utils/Constants";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { showNotification } from "../../actions/NotificationAction";
import { getWaitingApprovalList, deleteSavedSearch, changeProuctApproval } from "../../actions/searchAction";
import ReactPaginate from "react-paginate";

class waitingApproval extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userList: [],
      limit: 5,
      todosPerPage: 5,
      offset: 0,
      isModelOpen: 0,
      pageNo: 1,
      itemsPerPage: 5,
      total: 0,

      search: ""
    };
  }

  static propTypes = {
    prop: PropTypes
  };

  componentDidMount() {
    document.title = "Auto Harasow | Saved Search";
    window.scrollTo({
      top: 0,
      behavior: "smooth"
    });
    this.getApprovalList();
  }

  getApprovalList = () => {
    const { search, pageNo, itemsPerPage } = this.state;
    const params = {
      pageNo,
      itemsPerPage
    };
    this.props.getWaitingApprovalList(params, response => {
      console.log(response);
      if (response && response.response_code === 0) {
        const { totalRecords, pendingApprovalList } = response.response;
        this.setState({ total: totalRecords, userList: pendingApprovalList });
      }
    });
  };

  handlePageClick = data => {
    let selected = data.selected;
    let pageNo = Math.ceil(selected + 1);
    let offset = Math.ceil(selected * this.state.todosPerPage);
    this.setState({ offset: offset, pageNo }, () => {
      this.getApprovalList();
    });
  };

  changeProuctApproval = (vehicleId, approvedStatus) => {
    const params = {
      vehicleId,
      approvedStatus
    };
    this.props.changeProuctApproval(params, response => {
      if (response && response.response_code === 0) {
        this.props.showNotification("Updated successfully", "success");
        this.getApprovalList();
      } else {
        this.props.showNotification(response.response_message, "error");
      }
    });
  };

  deleteSavedSearch = (vehicleId, savedSearchId) => {
    let formData = new FormData();
    formData.append("vehicleId", vehicleId);
    formData.append("savedSearchId", savedSearchId);
    let data = {
      vehicleId: vehicleId,
      savedSearchId: savedSearchId
    };
    this.props.deleteSavedSearch(data, response => {
      console.log(response);
      if (response && response.response_code === 0) {
        this.props.showNotification("deleted successfully", "success");
        this.getuserList();
      } else {
        this.props.showNotification(response.response_message, "error");
      }
    });
  };

  searchDetails = vehicleId => {
    this.props.history.push({
      pathname: PATH.SEARCH_DETAIL,
      state: {
        vehicleId: vehicleId
      }
    });
  };

  render() {
    const { userList, total, todosPerPage, offset } = this.state;
    const pageDisplayCount = Math.ceil(total / todosPerPage);
    return (
      <React.Fragment>
        <section class="">
          <div class="container">
            <div class="row">
              <div class="col-md-9 form-wrap">
                <h1 class="form-header">PENDING APPROVAL List</h1>
                <p class="lead">List of pending approvals.</p>
                <div class="row no-gutters">
                  <div class="col-12">
                    <div class="table-responsive">
                      <table class="table table-striped">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Car Model</th>
                            <th scope="col">Description</th>
                            <th scope="col">Price</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          {userList && userList.length ? (
                            userList.map((list, index) => {
                              return (
                                <tr style={{ cursor: 'pointer' }}>
                                  <th scope="row">{offset + index + 1}</th>
                                  <td onClick={() => { this.searchDetails(list.vehicleId) }}>
                                    {list.vehicleName ? list.vehicleName : ""}
                                  </td>
                                  <td>
                                    {list.modelDetail} {list.conditionType}
                                  </td>
                                  <td>{list.price}</td>
                                  <td>
                                    <div
                                      class="btn-group"
                                      role="group"
                                      aria-label="Basic example"
                                    >
                                      <button
                                        type="button"
                                        class="btn btn-primary"
                                        onClick={() => { this.changeProuctApproval(list.vehicleId, 1) }}
                                      >
                                        Approve
                                      </button>
                                      <button
                                        type="button"
                                        class="btn btn-danger"
                                        onClick={() => { this.changeProuctApproval(list.vehicleId, -1) }}
                                      >
                                        Un Approve
                                      </button>
                                    </div>
                                  </td>
                                </tr>
                              );
                            })
                          ) : (
                              <tr className="text-center">
                                <td colspan="12">No items found</td>
                              </tr>
                            )}
                        </tbody>
                      </table>
                    </div>
                  </div>
                  {pageDisplayCount > 1 ? (
                    <div className="totalresults py-3 mt-3">
                      <div className="row align-items-center">
                        <div className="col-md-6">
                          <span className="bold">
                            {this.state.pageNo} - {pageDisplayCount}
                          </span>{" "}
                          out of <span className="bold">{pageDisplayCount}</span>{" "}
                          listings
                        </div>
                        <div className="col-md-6">
                          <ReactPaginate
                            previousLabel={"previous"}
                            nextLabel={"next"}
                            breakLabel={"..."}
                            breakClassName={"break-me"}
                            pageCount={pageDisplayCount}
                            marginPagesDisplayed={2}
                            pageRangeDisplayed={5}
                            onPageChange={this.handlePageClick}
                            containerClassName={
                              "pagination justify-content-end"
                            }
                            subContainerClassName={"page-item"}
                            activeClassName={"page-item active"}
                            pageLinkClassName={"page-link"}
                            nextLinkClassName={"page-link"}
                            previousLinkClassName={"page-link"}
                            nextClassName={"page-item"}
                            previousClassName={"page-item"}
                            disabledClassName={"disabled"}
                          />
                        </div>
                      </div>
                    </div>
                  ) : (
                      ""
                    )}
                </div>
              </div>
            </div>
          </div>
        </section>
      </React.Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getWaitingApprovalList: (params, callback) => {
      dispatch(getWaitingApprovalList(params, callback));
    },
    deleteSavedSearch: (params, callback) => {
      dispatch(deleteSavedSearch(params, callback));
    },
    changeProuctApproval: (params, callback) => {
      dispatch(changeProuctApproval(params, callback));
    },
    showNotification: (message, type) => {
      dispatch(showNotification(message, type));
    }
  };
};
export default connect(null, mapDispatchToProps)(waitingApproval);
